package edu.byu.hbll.hoverflydemo;

import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.created;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.equalsToJson;
import static io.specto.hoverfly.junit.verification.HoverflyVerifications.times;


import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import io.specto.hoverfly.junit.core.SimulationSource;
import io.specto.hoverfly.junit.dsl.HttpBodyConverter;
import io.specto.hoverfly.junit.rule.HoverflyRule;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import java.net.URI;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(HoverflyExtension.class)
@HoverflySimulate
public class Test2 {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private static final String BASE_URL = "https://lib.byu.edu";
  private static final String PATH = "/item/box/documents/";

  private static final String RESPONSE =
      "{\"recordId\":\"1\",\"title\":\"Some Book\","
          + "\"description\":\"It's a good book, I promise\"}";

  @ClassRule public static HoverflyRule hoverflyRule = HoverflyRule.inSimulationMode();

  @Before
  public void before() {
    // setup Hoverfly stubs
    hoverflyRule.simulate(
        SimulationSource.dsl(
            service(BASE_URL)

                .post("/item/box/documents")
                .body(equalsToJson(HttpBodyConverter.json(mapper.readTree(RESPONSE))))
                .willReturn(created(BASE_URL + PATH + "1"))

                .get(PATH + "1")
                .willReturn(success().body(HttpBodyConverter.json(mapper.readTree(RESPONSE))))));
  }

  @Test
  public void testPostMethod() {
    // create the record to post to the server
    ObjectNode node = mapper.createObjectNode();
    node.put("recordId", "1")
        .put("title", "Some Book")
        .put("description", "It's a good book, I promise");

    // test it
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response = restTemplate.postForEntity(
        BASE_URL + "/item/box/documents",
        mapper.writeValueAsString(node),
        String.class);

    // resource location
    URI resourceUrl = response.getHeaders().getLocation();

    System.out.println("\n\nServer created record:");
    System.out.println(resourceUrl);
    System.out.println();

    response = restTemplate.getForEntity(resourceUrl, String.class);

    System.out.println("Server returned record:");
    System.out.println(response.getBody());
    System.out.println();

    // verify HTTP request came through
    hoverflyRule.verify(service(BASE_URL)
        .post("/item/box/documents")
        .body(equalsToJson(HttpBodyConverter.json(mapper.readTree(RESPONSE)))), times(1));
    hoverflyRule.verify(service(BASE_URL).get(PATH + "1"), times(1));
  }
}
