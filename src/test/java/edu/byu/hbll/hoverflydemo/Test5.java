package edu.byu.hbll.hoverflydemo;

import static io.specto.hoverfly.junit.dsl.HoverflyDsl.response;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.equalsToJson;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.startsWith;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import io.specto.hoverfly.junit.core.SimulationSource;
import io.specto.hoverfly.junit.dsl.HttpBodyConverter;
import io.specto.hoverfly.junit.rule.HoverflyRule;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import java.util.Map;
import java.util.UUID;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@ExtendWith(HoverflyExtension.class)
@HoverflySimulate
public class Test5 {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private static final String BASE_URL = "https://lib.byu.edu";
  private static final String PATH = "/item/box/documents/";

  private static final String RESPONSE =
      "{\"recordId\":\"1\",\"title\":\"Some Book\","
          + "\"description\":\"It's a good book, I promise\"}";
  private static final String LOGIN_BODY = "{\"login\":\"USERNAME\",\"password\":\"PASSWORD\"}";
  private static final String LOGIN_RESPONSE = "{\"token\":\"%s\"}";

  @ClassRule
  public static HoverflyRule hoverflyRule = HoverflyRule.inSimulationMode();

  @Test
  public void testStateful() {
    hoverflyRule.simulate(
        SimulationSource.dsl(
            service(BASE_URL)
                .get(startsWith(PATH))
                .withState("authenticated", "false")
                .willReturn(response().status(401).body("{\"message\":\"not authorized\"}"))

                .get(PATH + "1")
                .withState("authenticated", "true")
                .willReturn(success().body(HttpBodyConverter.json(mapper.readTree(RESPONSE))))

                .post("/item/login")
                .body(equalsToJson(HttpBodyConverter.json(mapper.readTree(LOGIN_BODY))))
                .willReturn(success()
                    .body(HttpBodyConverter.json(mapper.readTree(
                        String.format(LOGIN_RESPONSE, UUID.randomUUID().toString()))))
                    .andSetState("authenticated", "true"))
    ));

    // you must set initial state AFTER registering the service stubs
    hoverflyRule.setState(Map.of("authenticated", "false"));

    RestTemplate restTemplate = new RestTemplate();

    // first test should return a 401
    ResponseEntity<String> response;
    try {
      response = restTemplate
        .getForEntity(BASE_URL + PATH + "1", String.class);
    } catch (HttpClientErrorException e) {
      System.out.println("\n\nFirst attempt:");
      System.out.println("Status code: " + e.getStatusCode());
      System.out.println(e.getResponseBodyAsString());
      System.out.println();
    }

    // login to the server
    ObjectNode loginNode = mapper.createObjectNode();
    loginNode.put("login", "USERNAME")
        .put("password", "PASSWORD");
    response = restTemplate.postForEntity(
        BASE_URL + "/item/login",
        mapper.writeValueAsString(loginNode),
        String.class);

    // extract the token
    JsonNode responseNode = mapper.readTree(response.getBody());
    String token = responseNode.path("token").asText(null);
    System.out.println("Logged in:");
    System.out.println(response.getBody());
    System.out.println();

    // try again with the token (in this case, it doesn't matter)
    HttpHeaders headers = new HttpHeaders();
    headers.add("token", token);
    response = restTemplate.getForEntity(BASE_URL + PATH + "1", String.class, headers);

    System.out.println("Second attempt:");
    System.out.println("Status code: " + response.getStatusCode());
    System.out.println(response.getBody());
    System.out.println();
  }
}
