package edu.byu.hbll.hoverflydemo;

import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.verification.HoverflyVerifications.times;

import io.specto.hoverfly.junit.core.SimulationSource;
import io.specto.hoverfly.junit.rule.HoverflyRule;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(HoverflyExtension.class)
@HoverflySimulate
public class Test1 {

  private static final String BASE_URL = "https://lib.byu.edu";
  private static final String PATH = "/item/box/documents/";

  private static final String RESPONSE =
      "{\"recordId\":\"1\",\"title\":\"Some Book\","
          + "\"description\":\"It's a good book, I promise\"}";

  @ClassRule public static HoverflyRule hoverflyRule = HoverflyRule.inSimulationMode();

  @Test
  public void basicTest() {
    // setup Hoverfly stubs
    hoverflyRule.simulate(
        SimulationSource.dsl(
            service(BASE_URL).get(PATH + "1").willReturn(success().body(RESPONSE))));

    // test it
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response =
        restTemplate.getForEntity(BASE_URL + PATH + "1", String.class);

    System.out.print("\n\n");
    System.out.println(response.getBody());
    System.out.println();

    // verify HTTP request came through
    hoverflyRule.verify(service(BASE_URL).get(PATH + "1"), times(1));
  }
}
