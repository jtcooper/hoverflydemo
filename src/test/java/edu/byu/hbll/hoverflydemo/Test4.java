package edu.byu.hbll.hoverflydemo;

import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.verification.HoverflyVerifications.times;


import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import io.specto.hoverfly.junit.core.SimulationSource;
import io.specto.hoverfly.junit.dsl.StubServiceBuilder;
import io.specto.hoverfly.junit.rule.HoverflyRule;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(HoverflyExtension.class)
@HoverflySimulate
public class Test4 {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private static final String BASE_URL = "https://lib.byu.edu";
  private static final String PATH = "/item/box/documents/";

  @ClassRule
  public static HoverflyRule hoverflyRule = HoverflyRule.inSimulationMode();

  @Test
  public void testWithStubServiceBuilder() {

    // StubServiceBuilder can be passed to other methods
    StubServiceBuilder serviceBuilder = service(BASE_URL);
    stubDocument(serviceBuilder, "1", "Some Book", "It's a good book, I promise");
    stubDocument(serviceBuilder, "2", "Harry Potter", "Wizardlings go to school");

    // register StubServiceBuilder with Hoverfly
    hoverflyRule.simulate(SimulationSource.dsl(serviceBuilder));

    // test it
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response = restTemplate
        .getForEntity(BASE_URL + PATH + "1", String.class);
    System.out.print("\n\n");
    System.out.println(response.getBody());
    System.out.println();

    response = restTemplate
        .getForEntity(BASE_URL + PATH + "2", String.class);
    System.out.println(response.getBody());

    // verify that HTTP requests came through
    hoverflyRule.verify(service(BASE_URL).get(PATH + "1"), times(1));
    hoverflyRule.verify(service(BASE_URL).get(PATH + "2"), times(1));
  }

  private void stubDocument(StubServiceBuilder serviceBuilder, String recordId, String title,
                            String description) {
    ObjectNode node = mapper.createObjectNode();
    node.put("recordId", recordId)
        .put("title", title)
        .put("description", description);
    serviceBuilder
        .get(PATH + recordId)
        .willReturn(success().body(mapper.writeValueAsString(node)));
  }
}
