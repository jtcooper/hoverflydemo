package edu.byu.hbll.hoverflydemo;

import static io.specto.hoverfly.junit.dsl.HoverflyDsl.response;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.created;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.notFound;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.any;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.equalsTo;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.equalsToJson;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.startsWith;
import static io.specto.hoverfly.junit.verification.HoverflyVerifications.times;


import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import io.specto.hoverfly.junit.core.SimulationSource;
import io.specto.hoverfly.junit.dsl.HttpBodyConverter;
import io.specto.hoverfly.junit.rule.HoverflyRule;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(HoverflyExtension.class)
@HoverflySimulate
public class Test3 {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private static final String BASE_URL = "https://lib.byu.edu";
  private static final String PATH = "/item/box/documents/";

  private static final String RESPONSE =
      "{\"recordId\":\"1\",\"title\":\"Some Book\","
          + "\"description\":\"It's a good book, I promise\"}";

  @ClassRule public static HoverflyRule hoverflyRule = HoverflyRule.inSimulationMode();

  @Before
  public void before() {
    // setup Hoverfly stubs
    hoverflyRule.simulate(
        SimulationSource.dsl(
            service(BASE_URL)

                // Query parameters
                .get(PATH)
                .queryParam("recordId", equalsTo("1"))
                .willReturn(success().body(RESPONSE))

                // Other types of responses
                .get(PATH)
                .willReturn(notFound().body("{\"message\":\"record not found\"}"))

                // Or pick your own status code
                .get(PATH)
                .willReturn(response().status(401).body("{\"message\":\"not authorized\"}"))

                // if you don't care about the query parameters but the request has at least one, you MUST do this
                .get(PATH)
                .anyQueryParams()
                .willReturn(success().body(RESPONSE))

                // request matchers
                .get(startsWith(PATH))
                .queryParam("recordId", any())
                .willReturn(success().body(RESPONSE))

                // super fuzzy matching
                .anyMethod(any())
                .anyQueryParams()
                .anyBody()
                .willReturn(success())

                // headers are ignored by default
                .get(PATH)
                .header("Content-Type", "application/json")
                .willReturn(success())
        ));
  }
}
